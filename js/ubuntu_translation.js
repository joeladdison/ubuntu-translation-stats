(function ($) {
  jQuery(document).ready(function(){
    function showTooltip(x, y, contents) {
      jQuery('<div id="flot-tooltip">' + contents + '</div>').css({
        position: 'absolute',
        display: 'none',
        top: y + 5,
        left: x + 5,
        border: '1px solid #fdd',
        padding: '2px',
        'background-color': '#fee',
        opacity: 0.80
      }).appendTo("body").fadeIn(200);
    }

    Drupal.flot.previousPoint = null;
    jQuery("#flot-ubuntu-translation").bind("plothover", function (event, pos, item) {

      if (item) {
        if (Drupal.flot.previousPoint != item.dataIndex) {
          Drupal.flot.previousPoint = item.dataIndex;

          jQuery("#flot-tooltip").remove();
          var x = item.datapoint[0],
              y = item.datapoint[1];

          var curr_date = new Date(x);
          var type = Drupal.flot.flot_ubuntu_translation.options.ut.type;
          var text = curr_date.toDateString() + " - ";
          if (type === 'untranslated') {
            text += "Untranslated: " + y;
          } else if (type === 'translated') {
            text += "Translated: " + y.toFixed(2) + "%";
          }
          showTooltip(item.pageX, item.pageY, text);
        }
      }
      else {
        jQuery("#flot-tooltip").remove();
        Drupal.flot.previousPoint = null;
      }
    });
  });
})(jQuery);
